import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
    templateUrl: './list-orders.component.html',
    styleUrls: ['./list-orders.component.scss']
})
export class ListOrdersComponent implements OnInit, AfterViewInit {
    public titulo: string;
    public cabecalho: string;
    public clicked: boolean;

    constructor() {
        this.clicked = false;
    }

    public ngOnInit(): void {
        this.titulo = 'Listagem de Pedidos';
        this.cabecalho = 'Pedidos';

        console.log('OnInit');
    }

    public ngAfterViewInit(): void {
        console.log('After View Init');
    }

    public onClick(clicked: boolean): void {
        this.clicked = clicked;
        this.cabecalho = 'Pedidos Clicked';
    }
}
