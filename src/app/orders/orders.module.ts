import { NgModule } from '@angular/core';

import { OrdersRoutingModule } from './orders-routing.module';
import { ListOrdersComponent } from './list-orders/list-orders.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        SharedModule,
        OrdersRoutingModule
    ],
    declarations: [ListOrdersComponent]
})
export class OrdersModule { }
