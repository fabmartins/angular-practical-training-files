import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnChanges {
    @Input()
    public cabecalho: string;

    @Output()
    public clicked: EventEmitter<boolean>;

    constructor() {
        this.clicked = new EventEmitter<boolean>();
    }

    public ngOnInit() {
    }

    public ngOnChanges(simpleObject: SimpleChanges): void {
        console.log(simpleObject);
    }

    public onClick(): void {
        this.clicked.emit(true);
    }
}
