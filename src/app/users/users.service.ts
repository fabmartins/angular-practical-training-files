import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user.interface';

import { environment } from '../../environments/environment';

@Injectable()
export class UsersService {

    constructor(private httpClient: HttpClient) { }

    public getUsers(): Observable<User[]> {
        return this.httpClient.get<User[]>(`${environment.apiURL}/users`);
    }
}
