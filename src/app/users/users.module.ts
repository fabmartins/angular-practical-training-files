import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { ListUsersComponent } from './list-users/list-users.component';
import { UsersService } from './users.service';

@NgModule({
    imports: [
        SharedModule,
        UsersRoutingModule
    ],
    declarations: [ListUsersComponent],
    providers: [UsersService]
})
export class UsersModule { }
